MarketPlace = new Meteor.Collection('marketPlace');
var MIN_CARD_IN_DECK = Meteor.settings.public.minCardInDeck;
var NEED_ADMIN_APPROVAL = Meteor.settings.public.needAdminApprovalInMarket;
Meteor.methods({
    postDeckToMarket: function(deckToMarket) {
        var user = Meteor.user();

        if(!user)
            throw new Meteor.Error(401, "You need to login to create new decks");
        if(!deckToMarket.title || !deckToMarket.originalDeckId || !deckToMarket.price)
            throw new Meteor.Error(422, "Please fill with a title");

        var originalDeck = Decks.findOne({_id: deckToMarket.originalDeckId});
        if(!originalDeck)
            throw new Meteor.Error(422, "Original deck can't be found");

        if(deckToMarket.cardCount < MIN_CARD_IN_DECK)
            throw new Meteor.Error(422, "Deck needs to have more than "+MIN_CARD_IN_DECK+" cards");

        var deck = _.extend(_.pick(deckToMarket, 'title', 'description', 'price', 'originalDeckId', 'type', 'cardCount'), {
            userId: user._id,
            authorId: user._id,
            author: user.username,
            submitted: new Date().getTime(),
            version: originalDeck.version,
            commentsCount: 0,
            votes: {},
            rating: 3,
            approved: !NEED_ADMIN_APPROVAL
        });

        return MarketPlace.insert(deck);
    },
    purchaseDeck: function(deckToMarket) {
        var user = Meteor.user();

        if(!user)
            throw new Meteor.Error(401, "You need to login to create new decks");

        if(!deckToMarket.title || !deckToMarket.originalDeckId || !deckToMarket.price)
            throw new Meteor.Error(422, "Please fill with a title");

        var originalMarketDeck = Decks.findOne({_id: deckToMarket.originalDeckId});

        if(!originalMarketDeck)
            throw new Meteor.Error(422, "Original deck can't be found");

        var deck = _.extend(_.pick(deckToMarket, 'title', 'description', 'price', 'originalDeckId', 'originalMarketDeckId', 'type', 'author', 'authorId'), {
            userId: user._id,
            submitted: new Date().getTime(),
            version: originalMarketDeck.version,
            countCard: 0
        });

        var purchase = {
            userId: user._id,
            userName: user.username,
            marketDeckId: deckToMarket.originalMarketDeckId
        };

        createPurchaseNotification(purchase);

        return Decks.insert(deck);
    },
    approveDeck: function(marketDeckId){
        //TODO: check for admin user here
        var user = Meteor.user();

        if(!user)
            throw new Meteor.Error(401, "You need to be admin to approve deck");

        var marketDeck = MarketPlace.findOne({_id: marketDeckId});
        if(!marketDeck)
            throw new Meteor.Error(401, "Can't find deck in market");

        marketDeck.approved = true;

        MarketPlace.update(marketDeckId, {$set: marketDeck}, function (error) {
            if (error) {
                throwError(error.reason);
                if (error.error === 302) {
                    //Router.go('postPage', {_id: error.details}, {});
                }
            }
        });
    }
});

MarketPlace.allow({
    insert: function(userId, doc) {
        return !!userId;
    },
    update: ownsDocument,
    remove: ownsDocument
});