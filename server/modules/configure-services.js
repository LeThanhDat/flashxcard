/**
 * Created by myticmoon on 12/9/16.
 */
const services = Meteor.settings.private.oAuth;

const configure = () => {
    if ( services ) {
        for( let service in services ) {
            ServiceConfiguration.configurations.upsert( { service: service }, {
                $set: services[ service ]
            });
        }
    }
};

Modules.server.configureServices = configure;