Template.customLogin.events({
    'click [data-social-login]' ( event, template ) {
        const service = event.target.getAttribute( 'data-social-login' ),
            options = {
                requestPermissions: [ 'email' ]
            };

        if ( service === 'loginWithTwitter' ) {
            delete options.requestPermissions;
        }

        Meteor[ service ]( options, ( error ) => {
            if ( error ) {
                sAlert.warning( error.message);
            }
        });
    },
    'click .btn-logout' ( event, template) {
        Meteor.logout((error) => {
            if (error) {
                sAlert.warning(error.message);
            }
        });
    }
});
