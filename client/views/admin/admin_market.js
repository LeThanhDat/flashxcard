Template.adminMarket.helpers({
    'marketDeckList': function() {
        var decks =  MarketPlace.find({type: "marketDeck"});
        return decks;
    },
    textQuery: function() {
        return Session.get('adminMarketMainTextQuery');
    }
});

Template.adminMarket.events({
    'keyup [name=search_field]': _.debounce(function(event, template) {
        event.preventDefault();
        Session.set('adminMarketMainTextQuery', template.find('[name=search_field]').value);
    }, 500)
});
