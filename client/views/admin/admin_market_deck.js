/**
 * Created by myticmoon on 9/10/16.
 */
Template.adminMarketDeck.helpers({
    'isAuthor': function() {
        return Meteor.userId() == this.userId;
    },
    'voteable': function() {
        return Meteor.userId();
    }
});

Template.adminMarketDeck.events({
    'click .approve-deck': function(e) {
        e.preventDefault();
        alert("working");
        if(confirm("Approve this card?")){
            var marketDeckId = Template.instance().data._id;
            Meteor.call('approveDeck', marketDeckId, function (error) {
                if(error) {
                    sAlert.warning(error.reason);
                    throwError(error.reason);
                }
                else {
                    sAlert.success("Approved deck successfully");
                }
            });
        }
    }
});